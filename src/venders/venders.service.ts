import { Injectable } from '@nestjs/common';
import { CreateVenderDto } from './dto/create-vender.dto';
import { UpdateVenderDto } from './dto/update-vender.dto';
import { Repository } from 'typeorm';
import { Vender } from './entities/vender.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class VendersService {
  constructor(
    @InjectRepository(Vender) private vendersRepository: Repository<Vender>,
  ) {}
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  create(createVenderDto: CreateVenderDto) {
    return 'This action adds a new vender';
  }
  findAll() {
    return this.vendersRepository.find();
  }

  findOne(id: number) {
    return this.vendersRepository.findOneOrFail({
      where: { id },
    });
  }

  findOneByTel(tel: string) {
    return this.vendersRepository.findOneOrFail({
      where: { tel },
    });
  }

  findOneByName(name: string) {
    return this.vendersRepository.findOneOrFail({ where: { name } });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateVenderDto: UpdateVenderDto) {
    return `This action updates a #${id} vender`;
  }

  async remove(id: number) {
    const deleteVender = await this.vendersRepository.findOneOrFail({
      where: { id },
    });
    await this.vendersRepository.remove(deleteVender);

    return deleteVender;
  }
}
