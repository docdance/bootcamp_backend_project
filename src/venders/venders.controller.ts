import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
} from '@nestjs/common';
import { VendersService } from './venders.service';
import { CreateVenderDto } from './dto/create-vender.dto';
import { UpdateVenderDto } from './dto/update-vender.dto';

@Controller('venders')
export class VendersController {
  constructor(private readonly vendersService: VendersService) {}

  @Post()
  create(@Body() createVenderDto: CreateVenderDto) {
    return this.vendersService.create(createVenderDto);
  }

  @Get()
  findAll() {
    return this.vendersService.findAll();
  }
  @Get('id/:id')
  findOne(@Param('id') id: string) {
    return this.vendersService.findOne(+id);
  }

  @Get('tel/:tel')
  findOneByTel(@Param('tel') tel: string) {
    return this.vendersService.findOneByTel(tel);
  }

  @Get('name/:name')
  findOneByName(@Param('name') name: string) {
    return this.vendersService.findOneByName(name);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVenderDto: UpdateVenderDto) {
    return this.vendersService.update(+id, updateVenderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vendersService.remove(+id);
  }
}
