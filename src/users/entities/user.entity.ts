import { BillPayment } from 'src/billPayments/entities/billPayment.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { CheckStock } from 'src/checkStock/entities/checkStock.entity';
import { CheckIn } from 'src/checkin/entities/checkin.entity';

import { PurOrder } from 'src/materialOrders/entities/purOrder.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Role } from 'src/roles/entities/role.entity';
// import { Salary } from 'src/salarys/entities/salary.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({
    name: 'full_name',
    default: '',
  })
  fullName: string;

  @Column()
  password: string;

  @Column()
  gender: string;

  @Column({
    name: 'tel_no',
    default: '',
  })
  tel: string;

  @Column({
    name: 'base_salary',
    default: 0,
  })
  baseSalary: number;

  @Column({
    name: 'bank_name',
    default: '',
  })
  bankName: string;

  @Column({
    name: 'bank_account',
    default: '',
  })
  bankAccount: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  status: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @OneToMany(() => CheckStock, (checkStock) => checkStock.user)
  checkStocks: CheckStock[];

  @OneToMany(() => PurOrder, (order) => order.user)
  purOrders: PurOrder[];

  @ManyToOne(() => Branch, (branch) => branch.users, { cascade: true })
  branchs: Branch;

  @OneToMany(() => BillPayment, (billPayment) => billPayment.user)
  billPayments: BillPayment[];

  @OneToMany(() => CheckIn, (checkin) => checkin.user)
  checkin: CheckIn[];

  // @OneToMany(() => Salary, (salary) => salary.user)
  // salary: Salary[];
}
