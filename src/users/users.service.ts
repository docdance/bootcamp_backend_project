import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
    @InjectRepository(Branch) private branchRepository: Repository<Branch>,
  ) {}
  // create(createUserDto: CreateUserDto) {
  //   return 'This action adds a new user';
  // }

  // findAll() {
  //   return this.usersRepository.find({
  //     relations: {
  //       roles: true,
  //     },
  //   });
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} user`;
  // }

  // findOneByEmail(email: string) {
  //   return this.usersRepository.findOneByOrFail({ email });
  // }

  // update(id: number, updateUserDto: UpdateUserDto) {
  //   return `This action updates a #${id} user`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} user`;
  // }

  create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;
    user.password = createUserDto.password;
    user.roles = JSON.parse(createUserDto.roles);
    user.branchs = JSON.parse(createUserDto.branch);
    user.baseSalary = createUserDto.baseSalary;
    user.bankName = createUserDto.bankName;
    user.bankAccount = createUserDto.bankAccount;
    user.status = createUserDto.status;
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({
      relations: { roles: true, branchs: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true, branchs: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOne({
      where: { email },
      relations: { roles: true, branchs: true },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.password = updateUserDto.password;
    user.tel = updateUserDto.tel;
    user.baseSalary = updateUserDto.baseSalary;
    user.bankName = updateUserDto.bankName;
    user.bankAccount = updateUserDto.bankAccount;
    user.roles = JSON.parse(updateUserDto.roles);
    user.branchs = JSON.parse(updateUserDto.branch);
    user.status = updateUserDto.status;

    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true, branchs: true },
    });
    // await this.usersRepository.update(id, {
    //   ...updateUser,
    //   ...updateUserDto,
    // });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.tel = user.tel;
    updateUser.baseSalary = user.baseSalary;
    updateUser.bankName = user.bankName;
    updateUser.bankAccount = user.bankAccount;
    updateUser.image = user.image;
    updateUser.branchs = user.branchs;
    updateUser.status = user.status;
    // updateUser.roles = updateUserDto.roles;
    // เพิ่มใหม่
    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }

    // for (const b of user.branchs) {
    //   const searchBranch = updateUser.branchs.find(
    //     (branch) => branch.id === b.id,
    //   );
    //   if (!searchBranch) {
    //     const newBranch = await this.branchRepository.findOneBy({ id: b.id });
    //     updateUser.branchs.push(newBranch);
    //   }
    // }
    // ลบออก
    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = updateUser.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );
      if (index < 0) {
        updateUser.roles.splice(index);
      }
    }

    // for (let i = 0; i < updateUser.branchs.length; i++) {
    //   const index = updateUser.branchs.findIndex(
    //     (branch) => branch.id === updateUser.branchs[i].id,
    //   );
    //   if (index < 0) {
    //     updateUser.branchs.splice(index);
    //   }
    // }
    await this.usersRepository.save(updateUser);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }

  async getUser(param1: string, param2: string) {
    const result = await this.usersRepository.query(
      `CALL getUser(${param1},${param2})`,
    );
    return result[0];
  }
}
