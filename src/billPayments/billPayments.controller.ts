import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { BillPaymentsService } from './billPayments.service';
import { CreateBillPaymentDto } from './dto/create-billPayment.dto';
import { UpdateBillPaymentDto } from './dto/update-billPayment.dto';

@Controller('billPayments')
export class BillPaymentsController {
  constructor(private readonly billPaymentsService: BillPaymentsService) {}

  @Post()
  create(@Body() createBillPaymentDto: CreateBillPaymentDto) {
    return this.billPaymentsService.create(createBillPaymentDto);
  }

  @Get()
  findAll() {
    return this.billPaymentsService.findAll();
  }

  @Get('User/:userId')
  findAllByUser(@Param('userId') userId: number) {
    return this.billPaymentsService.findAllByUser(userId);
  }

  @Get('User/:fullName')
  findAllByFullName(@Param('fullName') fullName: string) {
    return this.billPaymentsService.findAllByFullName(fullName);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.billPaymentsService.findOne(+id);
  }

  // @Get('TypeBillId/:TypeBillId')
  // findAllByTypeBill(@Param('TypeBillId') typeBill: string) {
  //   return this.billPaymentsService.findAllByTypeBill(typeBill);
  // }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBillPaymentDto: UpdateBillPaymentDto,
  ) {
    return this.billPaymentsService.update(+id, updateBillPaymentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.billPaymentsService.remove(+id);
  }
}
