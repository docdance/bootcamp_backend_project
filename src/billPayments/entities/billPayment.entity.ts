import { Branch } from 'src/branch/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinTable,
} from 'typeorm';

@Entity()
export class BillPayment {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ type: 'date' })
  date: string;

  // @ManyToOne(() => User, (user) => user.billPayments)
  // user: User;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column()
  typeBill: string;

  @ManyToOne(() => User, (user) => user.billPayments)
  @JoinTable()
  user: User[];

  @ManyToOne(() => Branch, (branch) => branch.billPayments, { cascade: true })
  @JoinTable()
  branchs: Branch[];

  // @ManyToOne(() => TypeBillPayment, (typeBillPay) => typeBillPay.typeBill)
  // @JoinColumn({ name: 'TypeBillId'})
  // typeBills: TypeBillPayment[];

  // @ManyToOne(() => TypeBillPayment, (typeBillPay) => typeBillPay.typeBill)
  // typeBills: TypeBillPayment;
}
