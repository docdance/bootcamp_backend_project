export class CreateBillPaymentDto {
  date: string;
  typeBill: string;
  user: string;
  total: number;
}
