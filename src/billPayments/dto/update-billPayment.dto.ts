import { PartialType } from '@nestjs/mapped-types';
import { CreateBillPaymentDto } from './create-billPayment.dto';

export class UpdateBillPaymentDto extends PartialType(CreateBillPaymentDto) {}
