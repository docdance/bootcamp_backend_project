import { Module } from '@nestjs/common';
import { BillPaymentsService } from './billPayments.service';
import { BillPaymentsController } from './billPayments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BillPayment } from './entities/billPayment.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([BillPayment, User])],
  controllers: [BillPaymentsController],
  providers: [BillPaymentsService],
})
export class BillPaymentsModule {}
