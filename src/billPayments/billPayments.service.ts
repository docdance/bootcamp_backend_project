import { Injectable } from '@nestjs/common';
import { CreateBillPaymentDto } from './dto/create-billPayment.dto';
import { UpdateBillPaymentDto } from './dto/update-billPayment.dto';
import { BillPayment } from './entities/billPayment.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class BillPaymentsService {
  constructor(
    @InjectRepository(BillPayment)
    private billPaymentRepository: Repository<BillPayment>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    // @InjectRepository(TypeBillPayment)
    // private typeBillPaymentRepository: Repository<TypeBillPayment>,
  ) {}

  // async create(createBillPaymentDto: CreateBillPaymentDto) {
  //   const billPayment = new BillPayment();

  //   const user = await this.usersRepository.findOneBy({
  //     id: createBillPaymentDto.userId,
  //   });
  //   createBillPaymentDto.userId = user.id;

  //   const type = await this.typeBillPaymentRepository.findOneBy({
  //     id: createBillPaymentDto.typeBill,
  //   });
  //   createBillPaymentDto.typeBillId = type.id;

  //   billPayment.user = user;
  //   billPayment.total = parseFloat(createBillPaymentDto.total);
  //   billPayment.date = new Date;
  //   billPayment.typeBills = type;

  //   return this.billPaymentRepository.save(billPayment)
  // }

  create(createBillPaymentDto: CreateBillPaymentDto) {
    const billPayment = new BillPayment();
    billPayment.date = createBillPaymentDto.date;
    billPayment.user = JSON.parse(createBillPaymentDto.user);
    billPayment.typeBill = JSON.parse(createBillPaymentDto.typeBill);
    billPayment.total = createBillPaymentDto.total;
    return this.billPaymentRepository.save(billPayment);
  }

  findAll() {
    return this.billPaymentRepository.find({
      relations: { user: true },
    });
  }

  findAllByUser(userId: number) {
    return this.billPaymentRepository.find({
      where: { user: { id: userId } },
      relations: { user: true },
    });
  }

  findAllByFullName(fullName: string) {
    return this.billPaymentRepository.find({
      where: { user: { fullName } },
      relations: { user: true },
    });
  }

  findOne(id: number) {
    return this.billPaymentRepository.findOne({
      where: { id },
      relations: { user: true },
    });
  }

  // findAllByBranch(branchId: number) {
  //   return this.billPaymentRepository.find({
  //     where: { branch: { id: branchId } },
  //     relations: ['user', 'branch', 'typeBill'],
  //     order: { created: 'ASC' },
  //   });
  // }

  // async update(id: number, updateBillPaymentDto: UpdateBillPaymentDto) {
  //   const billPayment = await this.billPaymentRepository.findOneOrFail({
  //     where: { id },
  //   });
  //   const type = await this.typeBillPaymentRepository.findOneBy({
  //     id: updateBillPaymentDto.typeBill,
  //   });
  //   updateBillPaymentDto.typeBill = type.id;

  //   const user = await this.usersRepository.findOneBy({
  //     id: updateBillPaymentDto.user,
  //   });
  //   updateBillPaymentDto.user = user.id;

  //   billPayment.typeBills = type;
  //   billPayment.total = parseFloat(updateBillPaymentDto.total);
  //   billPayment.user = user;
  //   billPayment.date = new Date(updateBillPaymentDto.date);
  //   this.billPaymentRepository.save(billPayment);

  //   await this.billPaymentRepository.save(billPayment);
  //   this.billPaymentRepository.save(billPayment);

  //   const result = await this.billPaymentRepository.findOne({
  //     where: { id },
  //     relations: { typeBills: true },
  //   });
  //   return result;
  // }

  // async update(id: number, updateBillPaymentDto: UpdateBillPaymentDto) {
  //   const billPayment = await this.billPaymentRepository.findOneOrFail({
  //     where: { id },
  //     relations: { typeBills: true, user: true },
  //   });

  //   // Check if typeBillId has changed before assigning
  //   if (billPayment.typeBills.id !== updateBillPaymentDto.typeBill) {
  //     const type = await this.typeBillPaymentRepository.findOneOrFail({
  //       where: { id: updateBillPaymentDto.typeBill },
  //     });
  //     billPayment.typeBills = type;
  //   }

  //   // Check if userId has changed before assigning
  //   if (billPayment.user.id !== updateBillPaymentDto.user) {
  //     const user = await this.usersRepository.findOneOrFail({
  //       where: { id: updateBillPaymentDto.user },
  //     });
  //     billPayment.user = user;
  //   }

  //   billPayment.total = parseFloat(updateBillPaymentDto.total);
  //   billPayment.date = new Date(updateBillPaymentDto.date);

  //   await this.billPaymentRepository.save(billPayment);

  //   // Re-fetching is not needed if you're not returning a DTO with updated relations
  //   // Unless you specifically need to return the refreshed entity from the database
  //   return billPayment;
  // }

  async update(id: number, updateBillPaymentDto: UpdateBillPaymentDto) {
    const billPayment = await this.billPaymentRepository.findOneOrFail({
      where: { id },
      relations: { user: true },
    });

    billPayment.date = updateBillPaymentDto.date;
    billPayment.user = JSON.parse(updateBillPaymentDto.user);
    billPayment.typeBill = JSON.parse(updateBillPaymentDto.typeBill);
    billPayment.total = updateBillPaymentDto.total;
    const updateBill = await this.billPaymentRepository.findOne({
      where: { id },
      relations: { user: true },
    });
    return updateBill;
  }

  async remove(id: number) {
    const deleteBillPayment = await this.billPaymentRepository.findOneOrFail({
      where: { id },
    });
    await this.billPaymentRepository.remove(deleteBillPayment);

    return deleteBillPayment;
  }
}
