export class CreateProductDto {
  name: string;

  price: string;

  sweet: string;

  image: string;

  type: string;
}
