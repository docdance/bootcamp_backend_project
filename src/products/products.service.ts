import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product = new Product();
    product.name = createProductDto.name;
    product.price = parseFloat(createProductDto.price);
    product.sweet = createProductDto.sweet;
    if (createProductDto.image && createProductDto.image !== '') {
      product.image = createProductDto.image;
    }
    console.log(createProductDto.type);
    product.type = JSON.parse(createProductDto.type);
    return this.productsRepository.save(product);
  }

  ///////////// save array of product
  async pushJSON(createProductDto: CreateProductDto[]) {
    for (let i = 0; i < createProductDto.length; i++) {
      const product = new Product();
      product.name = createProductDto[i].name;
      product.price = parseFloat(createProductDto[i].price);
      product.type = JSON.parse(createProductDto[i].type);
      product.image = createProductDto[i].name + '.png';
      product.sweet = createProductDto[i].sweet;
      await this.productsRepository.save(product);
    }
    return this.productsRepository.find();
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true } });
  }

  findAllByType(typeName: string) {
    return this.productsRepository.find({
      where: { type: { name: typeName } }, // find by type
      relations: { type: true },
      order: { name: 'ASC' },
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneOrFail({
      where: { id },
    });

    product.name = updateProductDto.name;
    product.price = parseFloat(updateProductDto.price);
    product.type = JSON.parse(updateProductDto.type);
    product.sweet = updateProductDto.sweet;
    if (updateProductDto.image && updateProductDto.image !== '') {
      product.image = updateProductDto.image;
    }

    this.productsRepository.save(product);

    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    await this.productsRepository.remove(deleteProduct);

    return deleteProduct;
  }

  async getProducteiei(param1: string, param2: string) {
    console.log(param1, param2);

    const result = await this.productsRepository.query(
      `CALL getProduct(${param1},'${param2}')`,
    );
    return result[0];
  }

  async getProduct4() {
    try {
      const result = await this.productsRepository.query(`CALL loopProduct();`);
      console.log(result);
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
}
