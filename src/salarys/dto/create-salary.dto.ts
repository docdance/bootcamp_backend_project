// import { CheckIn } from 'src/checkin/entities/checkin.entity';
// import { User } from 'src/users/entities/user.entity';

export class CreateSalaryDto {
  userId: number;
  pay: number;
  date: Date;
  salaryStatus: string;
}
