import { Module } from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { SalarysController } from './salarys.controller';
import { Salary } from './entities/salary.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Salary])],
  controllers: [SalarysController],
  providers: [SalarysService],
})
export class SalarysModule {}
