// import { CheckIn } from 'src/checkin/entities/checkin.entity';
// import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  // ManyToOne,
  // OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @Column()
  pay: number;

  @Column({ type: 'date' }) // Specify type as 'date' to store only the date component
  date: Date;

  @Column()
  salaryStatus: string;

  // @ManyToOne(() => User, (user) => user.salary)
  // user: User;

  // @OneToMany(() => CheckIn, (checkin) => checkin.salary)
  // checkin: CheckIn[];
}
