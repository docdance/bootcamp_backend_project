import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private SalarysRepository: Repository<Salary>,
  ) {}

  create(createSalaryDto: CreateSalaryDto) {
    const salary = new Salary();
    salary.userId = createSalaryDto.userId;
    salary.pay = createSalaryDto.pay;
    // แปลงค่าของ createSalaryDto.date เป็นชนิดข้อมูล Date
    const parsedDate = new Date(createSalaryDto.date);

    // กำหนดค่าให้กับฟิลด์ของอ็อบเจกต์ salary
    salary.date = parsedDate; // ใช้ค่าที่แปลงจาก string เป็น Date
    salary.salaryStatus = createSalaryDto.salaryStatus;
    // if (salary.quantity < salary.min) {
    //   createSalaryDto.status = 'Low';
    // } else {
    //   createSalaryDto.status = 'Available';
    // }
    // salary.status = createSalaryDto.status;

    // if (createSalaryDto.image && createSalaryDto.image !== '') {
    //   salary.image = createSalaryDto.image;
    // }

    return this.SalarysRepository.save(salary);
  }

  // findAllByStatus(Status: string) {
  //   return this.SalarysRepository.find({
  //     where: { status: Status }, // find by type
  //     order: { status: 'ASC' },
  //   });
  // }

  findAll() {
    return this.SalarysRepository.find();
  }

  findOne(id: number): Promise<Salary> {
    return this.SalarysRepository.findOneBy({ id });
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.SalarysRepository.findOneOrFail({
      where: { id },
    });
    const parsedDate = new Date(updateSalaryDto.date);
    salary.userId = updateSalaryDto.userId;
    salary.pay = updateSalaryDto.pay;
    salary.date = parsedDate;
    salary.salaryStatus = updateSalaryDto.salaryStatus;
    // salary.price = updateSalaryDto.price;
    // salary.quantity = updateSalaryDto.quantity;
    // salary.min = updateSalaryDto.min;
    // salary.use = salary.quantity - salary.qt_previous;
    // if (salary.quantity < salary.min) {
    //   updateSalaryDto.status = 'Low';
    //   salary.status = updateSalaryDto.status;
    // } else {
    //   updateSalaryDto.status = 'Available';
    //   salary.status = updateSalaryDto.status;
    // }

    await this.SalarysRepository.save(salary);
    // const updateSalary = await this.SalarysRepository.findOneOrFail({
    //   where: { id },
    // });
    // updateSalary.name = salary.name;
    // updateSalary.price = salary.price;
    // updateSalary.quantity = salary.quantity;
    // updateSalary.min = salary.min;
    // if (updateSalary.quantity < updateSalary.min) {
    //   salary.status = 'Low';
    //   updateSalary.status = salary.status;
    // } else {
    //   salary.status = 'Available';
    //   updateSalary.status = salary.status;
    // }
    // await this.SalarysRepository.save(updateSalary);
    const result = await this.SalarysRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteSalary = await this.SalarysRepository.findOneOrFail({
      where: { id },
    });
    await this.SalarysRepository.remove(deleteSalary);
    return deleteSalary;
  }
}
