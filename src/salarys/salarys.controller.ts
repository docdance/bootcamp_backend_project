import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
} from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
// import { FileInterceptor } from '@nestjs/platform-express';
// import { diskStorage } from 'multer';
// import { uuid } from 'uuidv4';
// import { extname } from 'path';

// @UseGuards(AuthGuard)
@Controller('salarys')
export class SalarysController {
  constructor(private readonly salarysService: SalarysService) {}
  // Create
  @Post()
  create(@Body() createSalaryDto: CreateSalaryDto) {
    return this.salarysService.create(createSalaryDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.salarysService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.salarysService.findOne(+id);
  }

  // @Get(':status')
  // findAllByStatus(Status: 'Low') {
  //   return this.salarysService.findAllByStatus(Status);
  // }
  // Partail Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSalaryDto: UpdateSalaryDto) {
    return this.salarysService.update(+id, updateSalaryDto);
  }

  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.salarysService.remove(+id);
  }
}
