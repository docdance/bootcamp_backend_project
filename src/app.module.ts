import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Product } from './products/entities/product.entity';
import { Promotion } from './promotion/entities/promotion.entity';
import { PromotionsModule } from './promotion/promotions.module';
import { ProductsModule } from './products/products.module';
import { Material } from './materials/entities/material.entity';
import { MaterialsModule } from './materials/materials.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { Role } from './roles/entities/role.entity';
import { AuthModule } from './auth/auth.module';
import { User } from './users/entities/user.entity';
import { RolesModule } from './roles/roles.module';
import { Type } from './types/entities/type.entity';
import { TypesModule } from './types/types.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { OrdersModule } from './orders/orders.module';
import { CheckStock } from './checkStock/entities/checkStock.entity';
import { StockDetail } from './checkStock/entities/stockDetail.entity';
import { CheckStockModule } from './checkStock/checkStock.module';
import { Vender } from './venders/entities/vender.entity';

import { OrderDetail } from './materialOrders/entities/orderDetail.entity';
import { VendersModule } from './venders/venders.module';
import { materialOrdersModule } from './materialOrders/materialOrders.module';
import { MembersModule } from './members/members.module';
import { Member } from './members/entities/member.entity';
import { BranchModule } from './branch/branch.module';
import { Branch } from './branch/entities/branch.entity';
import { StockReportModule } from './stock-report/stock-report.module';
import { BillPayment } from './billPayments/entities/billPayment.entity';
import { BillPaymentsModule } from './billPayments/billPayments.module';
import { PurOrder } from './materialOrders/entities/purOrder.entity';
import { CheckIn } from './checkin/entities/checkin.entity';
import { CheckInsModule } from './checkin/checkin.module';
import { SalarysModule } from './salarys/salarys.module';
import { Salary } from './salarys/entities/salary.entity';
import { ReportorderModule } from './reportorder/reportorder.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      logging: false,
      entities: [
        Promotion,
        Product,
        Material,
        User,
        Role,
        Type,
        Order,
        OrderItem,
        CheckStock,
        StockDetail,
        Vender,
        OrderDetail,
        Member,
        Branch,
        BillPayment,
        PurOrder,
        CheckIn,
        Salary,
      ],
      synchronize: true,
    }),
    // TypeOrmModule.forRoot({
    //   type: 'mysql',

    //   entities: [
    //     Promotion,
    //     Product,
    //     Material,
    //     User,
    //     Role,
    //     Type,
    //     Order,
    //     OrderItem,
    //     CheckStock,
    //     StockDetail,
    //     Vender,

    //     OrderDetail,
    //     Member,
    //     Branch,
    //     BillPayment,
    //     PurOrder,

    //     CheckIn,
    //     Salary,
    //   ],
    //   synchronize: true,
    // }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    ProductsModule,
    MaterialsModule,
    PromotionsModule,
    RolesModule,
    AuthModule,
    OrdersModule,
    TypesModule,
    CheckStockModule,
    materialOrdersModule,
    VendersModule,
    MembersModule,
    BranchModule,
    StockReportModule,
    BillPaymentsModule,
    CheckInsModule,
    SalarysModule,
    ReportorderModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
