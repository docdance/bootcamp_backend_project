import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { Member } from 'src/members/entities/member.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Member) private membersRepository: Repository<Member>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order = new Order();
    const user = await this.usersRepository.findOneBy({
      id: createOrderDto.userId,
    });
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];
    order.promotions = createOrderDto.promotions;
    if (createOrderDto.member) {
      order.member = createOrderDto.member;
      this.membersRepository.update(
        createOrderDto.member.id,
        createOrderDto.member,
      );
    }
    order.cash = createOrderDto.cash;
    order.getPoint = createOrderDto.getPoint;
    order.usePoint = createOrderDto.usePoint;
    order.discount = createOrderDto.discount;
    order.payment = createOrderDto.payment;
    order.branchs = createOrderDto.branch;
    for (const oi of createOrderDto.orderItemsIdQty) {
      const orderItem = new OrderItem();
      orderItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.sweet = oi.sweet;
      orderItem.qty = oi.qty;
      orderItem.total = orderItem.price * orderItem.qty;
      await this.orderItemsRepository.save(orderItem);
      order.orderItems.push(orderItem);
      order.total += orderItem.total;
      order.qty += orderItem.qty;
    }
    order.total -= createOrderDto.discount;
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.ordersRepository.find({
      relations: {
        orderItems: { product: true },
        user: true,
        member: true,
        promotions: true,
      },
    });
  }

  findAllByMember(id: number) {
    const res = this.ordersRepository.find({
      where: { member: { id } },
      relations: {
        orderItems: { product: true },
        user: true,
        member: true,
        promotions: true,
      },
    });
    return res;
  }

  // findAllByType(typeName: string) {
  //   return this.productsRepository.find({
  //     where: { type: { name: typeName } }, // find by type
  //     relations: { type: true },
  //     order: { name: 'ASC' },
  //   });
  // }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: {
        orderItems: { product: true },
        user: true,
        member: true,
        promotions: true,
      },
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrder);

    return deleteOrder;
  }

  async getOrderAll() {
    try {
      const result = await this.ordersRepository.query(
        `SELECT * FROM viewOrder;`,
      );
      console.log(result);
      return result;
    } catch (error) {
      console.log(error);
    }
  }
}
