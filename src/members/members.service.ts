import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}

  create(createMemberDto: CreateMemberDto) {
    return this.membersRepository.save(createMemberDto);
  }

  findAll() {
    return this.membersRepository.find({});
  }

  findOne(id: number) {
    return this.membersRepository.findOneOrFail({
      where: { id },
    });
  }

  findByEmail(email: string) {
    return this.membersRepository.findOneOrFail({
      where: { email },
    });
  }

  findOneByTel(tel: string) {
    return this.membersRepository.findOneOrFail({
      where: { tel },
    });
  }

  update(id: number, updateMemberDto: UpdateMemberDto) {
    return `This action updates a #${id} member`;
  }

  remove(id: number) {
    return `This action removes a #${id} member`;
  }
}
