import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class StockReportService {
  constructor(private dataSource: DataSource) {}

  reportUsed() {
    return this.dataSource.query(`SELECT MAX(in_date) AS latest_date,
    name,
    SUM(use) AS used
FROM stock_detail
GROUP BY in_date,
       name`);
  }

  reportQty() {
    return this.dataSource.query(`SELECT in_date,
    name,
    SUM(quantity) AS quantity
FROM stock_detail

GROUP BY in_date,
       name`);
  }

  reportTotalUsed() {
    return this.dataSource.query(`SELECT MAX(in_date) AS latest_date,
    SUM(use) AS used
FROM stock_detail
GROUP BY in_date`);
  }

  reportTotalQty() {
    return this.dataSource.query(`SELECT MAX(in_date) AS latest_date,
    SUM(quantity) AS quantity
FROM stock_detail
GROUP BY in_date`);
  }

  queryUsedYear(year: string) {
    return this.dataSource.query(`SELECT SUBSTR(in_date, 7, 4) AS year,
    name,
          SUM(use) AS used
    FROM stock_detail
    WHERE year = '${year}'
    GROUP BY year,name
    ORDER BY year DESC
    `);
  }

  queryUsedMonth(year: string, month: string) {
    return this.dataSource.query(`SELECT SUBSTR(in_date, 7, 4) AS year,
    SUBSTR(in_date, 4, 2) AS month,
     name,
           SUM(use) AS used
    FROM stock_detail
    WHERE year = '${year}' AND month = '${month}'
    GROUP BY in_date,name
    ORDER BY year DESC,month DESC
    `);
  }

  queryQtyYear(year: string) {
    return this.dataSource.query(`SELECT SUBSTR(in_date, 7, 4) AS year,
    name,
          SUM(quantity) AS quantity
    FROM stock_detail
    WHERE year = '${year}'
    GROUP BY year,name
    ORDER BY year DESC
    `);
  }

  queryQtyMonth(year: string, month: string) {
    return this.dataSource.query(`SELECT SUBSTR(in_date, 7, 4) AS year,
    SUBSTR(in_date, 4, 2) AS month,
     name,
           SUM(quantity) AS quantity
    FROM stock_detail
    WHERE year = '${year}' AND month = '${month}'
    GROUP BY in_date,name
    ORDER BY year DESC,month DESC
    `);
  }

  queryPurPriceTotal(year: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year, od.name,SUM(od.total) AS Totalprice
    FROM pur_order po
    JOIN order_detail od ON po.id = od.purOrderId
    WHERE '${year}'
    GROUP BY year,name`);
  }

  queryPurPriceYear(year: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year, od.name,SUM(od.total) AS Totalprice
    FROM pur_order po
    JOIN order_detail od ON po.id = od.purOrderId
    WHERE '${year}'
    GROUP BY year,name`);
  }

  queryPurPriceMonth(year: string, month: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year,SUBSTR(po.date,4,2) AS month, od.name,SUM(od.total) AS Totalprice
    FROM pur_order po
    JOIN order_detail od ON po.id = od.purOrderId
    WHERE year = '${year}' AND month = '${month}'
    GROUP BY year,month,name
    `);
  }
  queryPurPriceDay(year: string, month: string, day: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year,SUBSTR(po.date,4,2) AS month,SUBSTR(po.date,1,2) AS day, od.name,SUM(od.total) AS Totalprice
      FROM pur_order po
      JOIN order_detail od ON po.id = od.purOrderId
      WHERE year = '${year}' AND month = '${month}' AND day = '${day}'
      GROUP BY year,month,day,name
      `);
  }
  queryPurQtyYear(year: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year, od.name,SUM(od.quantity) AS TotalQuantity
        FROM pur_order po
        JOIN order_detail od ON po.id = od.purOrderId
        WHERE '${year}'
        GROUP BY year,name`);
  }

  queryPurQtyMonth(year: string, month: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year,SUBSTR(po.date,4,2) AS month, od.name,SUM(od.quantity) AS TotalQuantity
        FROM pur_order po
        JOIN order_detail od ON po.id = od.purOrderId
        WHERE year = '${year}' AND month = '${month}'
        GROUP BY year,month,name
        `);
  }
  queryPurQtyDay(year: string, month: string, day: string) {
    return this.dataSource
      .query(`SELECT SUBSTR(po.date,7,4) AS year,SUBSTR(po.date,4,2) AS month,SUBSTR(po.date,1,2) AS day, od.name,SUM(od.quantity) AS TotalQuantity
          JOIN order_detail od ON po.id = od.purOrderId
          WHERE year = '${year}' AND month = '${month}' AND day = '${day}'
          GROUP BY year,month,day,name
          `);
  }
}
