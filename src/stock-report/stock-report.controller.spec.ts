import { Test, TestingModule } from '@nestjs/testing';
import { StockReportController } from './stock-report.controller';

describe('StockReportController', () => {
  let controller: StockReportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockReportController],
    }).compile();

    controller = module.get<StockReportController>(StockReportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
