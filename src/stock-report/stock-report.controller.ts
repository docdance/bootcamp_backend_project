import { Controller, Get, Param } from '@nestjs/common';
import { StockReportService } from './stock-report.service';

@Controller('stock-report')
export class StockReportController {
  constructor(private stockReportService: StockReportService) {}
  @Get('/reportUsed')
  reportUsed() {
    return this.stockReportService.reportUsed();
  }

  @Get('/reportQty')
  reportQty() {
    return this.stockReportService.reportQty();
  }

  @Get('/reportTotalUsed')
  reportTotalUsed() {
    return this.stockReportService.reportTotalUsed();
  }

  @Get('/reportTotalQty')
  reportTotalQty() {
    return this.stockReportService.reportTotalQty();
  }

  @Get('/reportUsed/:year')
  queryUsedYear(@Param('year') year: string) {
    return this.stockReportService.queryUsedYear(year);
  }

  @Get('/reportUsed/:year/:month')
  queryUsedMonth(@Param('year') year: string, @Param('month') month: string) {
    return this.stockReportService.queryUsedMonth(year, month);
  }

  @Get('/reportQty/:year')
  queryQtyYear(@Param('year') year: string) {
    return this.stockReportService.queryQtyYear(year);
  }

  @Get('/reportQty/:year/:month')
  queryQtyMonth(@Param('year') year: string, @Param('month') month: string) {
    return this.stockReportService.queryQtyMonth(year, month);
  }

  @Get('/reportPurPrice/:year/:month/:day')
  queryPurPriceDay(
    @Param('year') year: string,
    @Param('month') month: string,
    @Param('day') day: string,
  ) {
    return this.stockReportService.queryPurPriceDay(year, month, day);
  }

  @Get('/reportPurPrice/:year/:month')
  queryPurPriceMonth(
    @Param('year') year: string,
    @Param('month') month: string,
  ) {
    return this.stockReportService.queryPurPriceMonth(year, month);
  }

  @Get('/reportPurPrice/:year')
  queryPurPriceYear(@Param('year') year: string) {
    return this.stockReportService.queryPurPriceYear(year);
  }

  @Get('/reportPurQty/:year/:month/:day')
  queryPurQtyDay(
    @Param('year') year: string,
    @Param('month') month: string,
    @Param('day') day: string,
  ) {
    return this.stockReportService.queryPurQtyDay(year, month, day);
  }

  @Get('/reportPurQty/:year/:month')
  queryPurQtyMonth(@Param('year') year: string, @Param('month') month: string) {
    return this.stockReportService.queryPurQtyMonth(year, month);
  }

  @Get('/reportPurQty/:year')
  queryPurQtyYear(@Param('year') year: string) {
    return this.stockReportService.queryPurQtyYear(year);
  }
}
