import { Injectable, UnauthorizedException } from '@nestjs/common';

import { JwtService } from '@nestjs/jwt';
import { MembersService } from 'src/members/members.service';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private membersService: MembersService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, pass: string): Promise<any> {
    try {
      // Check if the provided credentials belong to a user
      const user = await this.usersService.findOneByEmail(email);
      if (user && user.password === pass) {
        console.log(user);
        const payload = { id: user.id, email: user.email };
        const { password, ...result } = user;
        return {
          user: result,
          access_token: await this.jwtService.signAsync(payload),
        };
      }

      // Check if the provided credentials belong to a member
      const member = await this.membersService.findByEmail(email);

      if (member && member.password === pass) {
        console.log(member);
        const payload = { id: member.id, email: member.email };
        const { password, ...result } = member;
        return {
          member: result,
          access_token: await this.jwtService.signAsync(payload),
        };
      }

      // If no user or member is found with the provided credentials, throw UnauthorizedException
      throw new UnauthorizedException();
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
}
