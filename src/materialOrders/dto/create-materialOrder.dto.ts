import { Vender } from 'src/venders/entities/vender.entity';

export class CreateMatOrderDto {
  purOrderDetails: {
    materialId: number;
    quantity: number;
  }[];
  vender: Vender;
  userId: number;
}
