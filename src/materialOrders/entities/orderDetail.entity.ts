import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PurOrder } from './purOrder.entity';
import { Material } from 'src/materials/entities/material.entity';

@Entity()
export class OrderDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  quantity: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => PurOrder, (purOrder) => purOrder.orderDetails, {
    onDelete: 'CASCADE',
  })
  purOrder: PurOrder;

  @ManyToOne(() => Material, (material) => material.matOrderDetail)
  material: Material;
}
