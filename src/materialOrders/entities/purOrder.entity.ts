import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { User } from 'src/users/entities/user.entity';
import { OrderDetail } from './orderDetail.entity';
import { Vender } from 'src/venders/entities/vender.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Entity()
export class PurOrder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  total: number;

  @Column()
  quantity: number;

  @Column()
  nameStore: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Vender, (vender) => vender.purOrders)
  vender: Vender;

  @OneToMany(() => OrderDetail, (orderDetail) => orderDetail.purOrder)
  orderDetails: OrderDetail[];

  @ManyToOne(() => User, (user) => user.purOrders)
  user: User;

  @ManyToOne(() => Branch, (branch) => branch.purOders, { cascade: true })
  @JoinTable()
  branchs: Branch[];
}
