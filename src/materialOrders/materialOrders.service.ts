import { Injectable } from '@nestjs/common';
import { CreateMatOrderDto } from './dto/create-materialOrder.dto';
import { UpdateMatOrderDto } from './dto/update-materialOrder.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { PurOrder } from './entities/purOrder.entity';
import { Repository } from 'typeorm';
import { OrderDetail } from './entities/orderDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Vender } from 'src/venders/entities/vender.entity';
import { Material } from 'src/materials/entities/material.entity';

@Injectable()
export class matOrdersService {
  constructor(
    @InjectRepository(PurOrder)
    private ordersRepository: Repository<PurOrder>,
    @InjectRepository(OrderDetail)
    private orderDetailsRepository: Repository<OrderDetail>,
    @InjectRepository(Vender) private vendersRepository: Repository<Vender>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  async create(createMatOrderDto: CreateMatOrderDto) {
    const purOrder = new PurOrder();
    const currentDate = new Date();
    const formattedDate = currentDate.toLocaleDateString('en-GB');
    const user = await this.usersRepository.findOneBy({
      id: createMatOrderDto.userId,
    });

    // const vender = await this.vendersRepository.findOneBy({
    //   id: createMatOrderDto.venderId,
    // });

    if (createMatOrderDto.vender) {
      purOrder.vender = createMatOrderDto.vender;
      this.vendersRepository.update(
        createMatOrderDto.vender.id,
        createMatOrderDto.vender,
      );
    }

    purOrder.user = user;
    purOrder.total = 0;
    purOrder.quantity = 0;
    purOrder.date = formattedDate;

    purOrder.nameStore = 'Coffee';
    purOrder.orderDetails = [];

    console.log('createMatOrderDto', createMatOrderDto.purOrderDetails);

    for (const oi of createMatOrderDto.purOrderDetails) {
      const orderDetail = new OrderDetail();
      orderDetail.material = await this.materialsRepository.findOneBy({
        id: oi.materialId,
      });

      orderDetail.name = orderDetail.material.name;
      orderDetail.price = orderDetail.material.price;
      orderDetail.quantity = oi.quantity;
      orderDetail.total = orderDetail.price * orderDetail.quantity;
      await this.orderDetailsRepository.save(orderDetail);
      purOrder.orderDetails.push(orderDetail);
      purOrder.total += orderDetail.total;
      purOrder.quantity += orderDetail.quantity;
    }
    return this.ordersRepository.save(purOrder);
  }

  findAll() {
    return this.ordersRepository.find({
      relations: {
        orderDetails: { material: true },
        user: true,
        vender: true,
      },
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: {
        orderDetails: { material: true },
        user: true,
        vender: true,
      },
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updatematerialOrderDto: UpdateMatOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deletematerialOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deletematerialOrder);

    return deletematerialOrder;
  }
}
