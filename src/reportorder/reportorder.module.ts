import { Module } from '@nestjs/common';
import { ReportorderController } from './reportorder.controller';
import { ReportorderService } from './reportorder.service';

@Module({
  controllers: [ReportorderController],
  providers: [ReportorderService],
})
export class ReportorderModule {}
