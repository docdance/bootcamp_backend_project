import { PartialType } from '@nestjs/mapped-types';
import { CreateReportorderDto } from './create-reportorder.dto';
export class UpdateReportorderDto extends PartialType(CreateReportorderDto) {}
