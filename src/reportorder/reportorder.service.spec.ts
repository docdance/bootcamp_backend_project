import { Test, TestingModule } from '@nestjs/testing';
import { ReportorderService } from './reportorder.service';

describe('ReportorderService', () => {
  let service: ReportorderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportorderService],
    }).compile();

    service = module.get<ReportorderService>(ReportorderService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
