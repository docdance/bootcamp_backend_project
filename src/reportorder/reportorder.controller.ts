import { Controller, Get, Param, Post } from '@nestjs/common';
import { ReportorderService } from './reportorder.service';

@Controller('reportorder')
export class ReportorderController {
  constructor(private reportorderService: ReportorderService) {}

  @Get('/Day')
  reportday() {
    return this.reportorderService.reportday();
  }

  @Get('/Month')
  reportmonth() {
    return this.reportorderService.reportmonth();
  }

  @Get('/Branch')
  reportbranch1() {
    return this.reportorderService.reportBranch();
  }

  @Get('/BestSell')
  reportBestSell() {
    return this.reportorderService.reportBestSell();
  }
  @Get('/Year')
  reportYear() {
    return this.reportorderService.reportYear();
  }

  @Get('/DayBranch')
  reportDayBranch() {
    return this.reportorderService.reportDayBranch();
  }

  @Get('/Bill')
  reportBill() {
    return this.reportorderService.reportBill();
  }

  @Get('/User')
  reportUser() {
    return this.reportorderService.reportUser();
  }
}
