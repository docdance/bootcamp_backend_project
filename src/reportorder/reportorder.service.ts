import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportorderService {
  constructor(private dataSource: DataSource) {}

  reportday() {
    return this.dataSource.query(`SELECT
    DATE(created) AS sale_date,
    SUM(total) AS total_sales
FROM
    "order"
GROUP BY
    DATE(created);

    `);
  }

  reportmonth() {
    return this.dataSource.query(`SELECT
    strftime('%Y-%m', created) AS sale_month,
    SUM(total) AS total_sales
FROM
    "order"
GROUP BY
    strftime('%Y-%m', created);

    `);
  }

  reportBranch() {
    return this.dataSource.query(`SELECT
    branch.name AS branch_name,
    SUM("order".total) AS total_sales
FROM
    "order"
JOIN
    branch ON "order".branchsId = branch.id
GROUP BY
    branch_name;
    `);
  }

  reportBestSell() {
    return this.dataSource.query(`SELECT
    product.name AS product_name,
    COUNT(order_item.id) AS total_sales
FROM
    order_item
JOIN
    product ON order_item.productId = product.id
GROUP BY
    product_name
ORDER BY
    total_sales DESC
LIMIT 5;
    `);
  }

  reportYear() {
    return this.dataSource.query(`SELECT
    strftime('%Y', created) AS sale_year,
    SUM(total) AS total_sales
FROM
    "order"
GROUP BY
    sale_year
ORDER BY
    sale_year;
    `);
  }
  reportDayBranch() {
    return this.dataSource.query(`SELECT
    branch.name AS branch_name,
    DATE("order".created) AS sale_date,
    SUM("order".total) AS total_sales
FROM
    "order"
JOIN
    branch ON "order".branchsId = branch.id
GROUP BY
    branch_name, sale_date;

    `);
  }
  reportBill() {
    return this.dataSource.query(`SELECT
    typeBill,
    SUM(total) AS total_amount
FROM
    bill_payment
GROUP BY
    typeBill;`);
  }
  reportUser() {
    return this.dataSource.query(`SELECT u.full_name AS user_name,
    SUM(o.total) AS total_sales
FROM [order] o
JOIN user u ON o.userId = u.id
GROUP BY u.full_name;
`);
  }
}
