import { Test, TestingModule } from '@nestjs/testing';
import { ReportorderController } from './reportorder.controller';

describe('ReportorderController', () => {
  let controller: ReportorderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportorderController],
    }).compile();

    controller = module.get<ReportorderController>(ReportorderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
