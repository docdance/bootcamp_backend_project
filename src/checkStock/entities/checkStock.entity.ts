import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { StockDetail } from './stockDetail.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  totalPrice: number;

  @Column()
  totalQty: number;

  @Column()
  totalUse: number;

  @Column()
  userName: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => StockDetail, (stockDetail) => stockDetail.checkStock)
  stockDetails: StockDetail[];

  @ManyToOne(() => User, (user) => user.checkStocks)
  user: User;
}
