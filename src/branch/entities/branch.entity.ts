import { BillPayment } from 'src/billPayments/entities/billPayment.entity';
import { PurOrder } from 'src/materialOrders/entities/purOrder.entity';
import { Order } from 'src/orders/entities/order.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Order, (order) => order.branchs)
  orders: Order[];

  @OneToMany(() => User, (user) => user.branchs)
  users: User[];

  @OneToMany(() => PurOrder, (purOrder) => purOrder.branchs)
  purOders: PurOrder[];

  @OneToMany(() => BillPayment, (billPayment) => billPayment.branchs)
  billPayments: BillPayment[];
}
