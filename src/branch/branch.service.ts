import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BranchService {
  constructor(
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
  ) {}
  create(createBranchDto: CreateBranchDto) {
    const branch = new Branch();
    branch.name = createBranchDto.name;
    branch.address = createBranchDto.address;
    return this.branchsRepository.save(createBranchDto);
  }

  findAll() {
    return this.branchsRepository.find();
  }

  findOne(id: number) {
    return this.branchsRepository.findOneBy({ id });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.branchsRepository.findOneByOrFail({ id });
    await this.branchsRepository.update(id, updateBranchDto);
    const updatedBranch = await this.branchsRepository.findOneBy({ id });
    return updatedBranch;
  }

  async remove(id: number) {
    const removedBranch = await this.branchsRepository.findOneByOrFail({ id });
    await this.branchsRepository.remove(removedBranch);
    return removedBranch;
  }

  async inseartBranch(name: string, address: string) {
    await this.branchsRepository.query(`CALL insertBranch(?, ?, @p2, @p3);`, [
      name,
      address,
    ]);
    const result = await this.branchsRepository.query(
      `SELECT @p2 AS pResult, @p3 AS pMessage;`,
    );
    return result[0]; // Accessing the first row of the result set
  }
}
