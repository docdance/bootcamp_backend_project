import { Test, TestingModule } from '@nestjs/testing';
import { CheckinsController } from './checkin.controller';
import { CheckInsService } from './checkin.service';

describe('CheckInsController', () => {
  let controller: CheckinsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckinsController],
      providers: [CheckInsService],
    }).compile();

    controller = module.get<CheckinsController>(CheckinsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
