import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  // ManyToOne,
  OneToMany,
} from 'typeorm';
import { User } from 'src/users/entities/user.entity';
// import { Salary } from 'src/salarys/entities/salary.entity';

@Entity()
export class CheckIn {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  checkin: Date;

  @CreateDateColumn()
  checkout: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => User, (user) => user.checkin)
  user: User;

  // @ManyToOne(() => Salary, (salary) => salary.checkin)
  // salary: Salary;
}
