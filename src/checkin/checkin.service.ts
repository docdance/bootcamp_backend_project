import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateCheckInDto } from './dto/create-checkin.dto';
import { UpdateCheckInDto } from './dto/update-checkin.dto';
import { CheckIn } from './entities/checkin.entity';

@Injectable()
export class CheckInsService {
  constructor(
    @InjectRepository(CheckIn) private checkinRepository: Repository<CheckIn>,
  ) {}
  create(createCheckInDto: CreateCheckInDto) {
    return this.checkinRepository.save(createCheckInDto);
  }

  findAll() {
    return this.checkinRepository.find({
      relations: { user: { roles: true } },
    });
  }

  findOne(id: number) {
    return this.checkinRepository.findOne({
      where: { id },
      relations: { user: { roles: true } },
    });
  }

  async update(id: number, updateCheckInDto: UpdateCheckInDto) {
    await this.checkinRepository.findOneByOrFail({ id });
    await this.checkinRepository.update(id, updateCheckInDto);
    const updatedCheckIn = await this.checkinRepository.findOneBy({ id });
    return updatedCheckIn;
  }

  async remove(id: number) {
    const removedCheckIn = await this.checkinRepository.findOneByOrFail({ id });
    await this.checkinRepository.remove(removedCheckIn);
    return removedCheckIn;
  }
}
