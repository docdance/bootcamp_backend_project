import { Module } from '@nestjs/common';
import { CheckinsController } from './checkin.controller';
import { CheckIn } from './entities/checkin.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckInsService } from './checkin.service';

@Module({
  imports: [TypeOrmModule.forFeature([CheckIn])],
  controllers: [CheckinsController],
  providers: [CheckInsService],
})
export class CheckInsModule {}
