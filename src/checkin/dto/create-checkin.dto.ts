import { User } from 'src/users/entities/user.entity';

export class CreateCheckInDto {
  checkin: Date;
  checkout: Date;
  updated: Date;
  user: User;
}
