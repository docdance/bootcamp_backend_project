// import {
//   Controller,
//   Get,
//   Post,
//   Body,
//   Patch,
//   Param,
//   Delete,
//   UseGuards,
// } from '@nestjs/common';
// import { CheckInsService } from './checkin.service';
// import { AuthGuard } from 'src/auth/auth.guard';
// import { CreateCheckInDto } from './dto/create-checkin.dto';
// import { UpdateCheckInDto } from './dto/update-checkin.dto';

// @UseGuards(AuthGuard)
// @Controller('checkins')
// export class CheckinsController {
//   constructor(private readonly checkinsService: CheckInsService) {}

//   @Post()
//   create(@Body() createCheckinDto: CreateCheckInDto) {
//     return this.checkinsService.create(createCheckinDto);
//   }

//   @Get()
//   findAll() {
//     return this.checkinsService.findAll();
//   }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.checkinsService.findOne(+id);
//   }

//   @Patch(':id')
//   update(@Param('id') id: string, @Body() updateCheckinDto: UpdateCheckInDto) {
//     return this.checkinsService.update(+id, updateCheckinDto);
//   }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.checkinsService.remove(+id);
//   }
// }

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CheckInsService } from './checkin.service';
import { AuthGuard } from 'src/auth/auth.guard';
import { CreateCheckInDto } from './dto/create-checkin.dto';
import { UpdateCheckInDto } from './dto/update-checkin.dto';

@UseGuards(AuthGuard)
@Controller('checkins')
export class CheckinsController {
  constructor(private readonly checkinsService: CheckInsService) {}

  @Post()
  create(@Body() createCheckinDto: CreateCheckInDto) {
    // เพิ่มการบันทึกข้อมูล check-in ในฐานข้อมูล
    return this.checkinsService.create(createCheckinDto);
  }

  @Get()
  findAll() {
    return this.checkinsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkinsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCheckinDto: UpdateCheckInDto) {
    return this.checkinsService.update(+id, updateCheckinDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkinsService.remove(+id);
  }
}
